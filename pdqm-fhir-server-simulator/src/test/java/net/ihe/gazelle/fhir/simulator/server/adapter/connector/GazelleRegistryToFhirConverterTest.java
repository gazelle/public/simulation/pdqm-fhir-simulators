package net.ihe.gazelle.fhir.simulator.server.adapter.connector;


import net.ihe.gazelle.app.patientregistryapi.business.*;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.Patient;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class GazelleRegistryToFhirConverterTest {

    @Test
    void TestPatientGenderConversion() {

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat1 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        pat1.setGender(GenderCode.FEMALE);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat2 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        pat2.setGender(GenderCode.MALE);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat3 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        pat3.setGender(GenderCode.OTHER);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat4 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        pat4.setGender(GenderCode.UNDEFINED);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat5 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        pat5.setGender(null);

        try {
            Patient response = GazelleRegistryToFhirConverter.convertPatient(pat1);
            assertEquals(Enumerations.AdministrativeGender.FEMALE, response.getGender());

            response = GazelleRegistryToFhirConverter.convertPatient(pat2);
            assertEquals(Enumerations.AdministrativeGender.MALE, response.getGender());

            response = GazelleRegistryToFhirConverter.convertPatient(pat3);
            assertEquals(Enumerations.AdministrativeGender.OTHER, response.getGender());

            response = GazelleRegistryToFhirConverter.convertPatient(pat4);
            assertEquals(Enumerations.AdministrativeGender.UNKNOWN, response.getGender());

        } catch (ConversionException e) {
            fail("could not convert Patient with just a gender");
        }

    }

    @Test
    void TestPatientAddressConversion() {

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        Address address = new Address();
        address.setCity("Chicoutimi");
        address.setCountryIso3("Canada");
        address.setPostalCode("G7B 0H3");
        address.setUse(AddressUse.WORK);
        address.setState("Québec");
        address.addLine("4 boulevard de l'université");
        pat.addAddress(address);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat1 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        Address address1 = new Address();
        address1.setUse(AddressUse.PRIMARY_HOME);
        pat1.addAddress(address1);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat2 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        Address address2 = new Address();
        address2.setUse(AddressUse.TEMPORARY);
        pat2.addAddress(address2);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat3 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        Address address3 = new Address();
        address3.setUse(AddressUse.BAD);
        pat3.addAddress(address3);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat4 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        Address address4 = new Address();
        address4.setUse(AddressUse.BILLING);
        pat4.addAddress(address4);

        try {
            Patient response = GazelleRegistryToFhirConverter.convertPatient(pat);
            org.hl7.fhir.r4.model.Address responseAddress = response.getAddress().get(0);
            assertEquals("Chicoutimi", responseAddress.getCity());
            assertEquals("Canada", responseAddress.getCountry());
            assertEquals("G7B 0H3", responseAddress.getPostalCode());
            assertEquals("Québec", responseAddress.getState());
            assertEquals("4 boulevard de l'université", responseAddress.getLine().get(0).getValue());
            assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.WORK, responseAddress.getUse());

            response = GazelleRegistryToFhirConverter.convertPatient(pat1);
            responseAddress = response.getAddress().get(0);
            assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.HOME, responseAddress.getUse());

            response = GazelleRegistryToFhirConverter.convertPatient(pat2);
            responseAddress = response.getAddress().get(0);
            assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.TEMP, responseAddress.getUse());

            response = GazelleRegistryToFhirConverter.convertPatient(pat3);
            responseAddress = response.getAddress().get(0);
            assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.OLD, responseAddress.getUse());

            response = GazelleRegistryToFhirConverter.convertPatient(pat4);
            responseAddress = response.getAddress().get(0);
            assertEquals(org.hl7.fhir.r4.model.Address.AddressUse.BILLING, responseAddress.getUse());

        } catch (ConversionException e) {
            fail("could not convert Patient with just an address");
        }

    }

    @Test
    void TestPatientTelecomConversion() {

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        ContactPoint cp = new ContactPoint();
        cp.setType(ContactPointType.BEEPER);
        cp.setValue("Hello");
        cp.setUse(ContactPointUse.WORK);
        pat.addContactPoint(cp);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat1 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        ContactPoint cp1 = new ContactPoint();
        cp1.setType(ContactPointType.PHONE);
        cp1.setValue("Hello");
        cp1.setUse(ContactPointUse.MOBILE);
        pat1.addContactPoint(cp1);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat2 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        ContactPoint cp2 = new ContactPoint();
        cp2.setType(ContactPointType.FAX);
        cp2.setValue("Hello");
        cp2.setUse(ContactPointUse.TEMPORARY);
        pat2.addContactPoint(cp2);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat3 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        ContactPoint cp3 = new ContactPoint();
        cp3.setType(ContactPointType.URL);
        cp3.setValue("Hello");
        cp3.setUse(ContactPointUse.HOME);
        pat3.addContactPoint(cp3);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat4 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        ContactPoint cp4 = new ContactPoint();
        cp4.setType(ContactPointType.EMAIL);
        cp4.setValue("Hello");
        cp4.setUse(ContactPointUse.PRIMARY_HOME);
        pat4.addContactPoint(cp4);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat5 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        ContactPoint cp5 = new ContactPoint();
        cp5.setType(ContactPointType.SMS);
        cp5.setValue("Hello");
        cp5.setUse(ContactPointUse.PRIMARY_HOME);
        pat5.addContactPoint(cp5);

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat6 = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        ContactPoint cp6 = new ContactPoint();
        cp6.setType(ContactPointType.OTHER);
        cp6.setValue("Hello");
        cp6.setUse(ContactPointUse.PRIMARY_HOME);
        pat6.addContactPoint(cp6);

        try {
            Patient response = GazelleRegistryToFhirConverter.convertPatient(pat);
            assertEquals("Hello", response.getTelecom().get(0).getValue());
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.WORK, response.getTelecom().get(0).getUse());
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.PAGER, response.getTelecom().get(0).getSystem());

            response = GazelleRegistryToFhirConverter.convertPatient(pat1);
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.MOBILE, response.getTelecom().get(0).getUse());
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.PHONE, response.getTelecom().get(0).getSystem());

            response = GazelleRegistryToFhirConverter.convertPatient(pat2);
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.TEMP, response.getTelecom().get(0).getUse());
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.FAX, response.getTelecom().get(0).getSystem());

            response = GazelleRegistryToFhirConverter.convertPatient(pat3);
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.HOME, response.getTelecom().get(0).getUse());
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.URL, response.getTelecom().get(0).getSystem());

            response = GazelleRegistryToFhirConverter.convertPatient(pat4);
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.HOME, response.getTelecom().get(0).getUse());
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.EMAIL, response.getTelecom().get(0).getSystem());

            response = GazelleRegistryToFhirConverter.convertPatient(pat5);
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.HOME, response.getTelecom().get(0).getUse());
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.SMS, response.getTelecom().get(0).getSystem());

            response = GazelleRegistryToFhirConverter.convertPatient(pat6);
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.HOME, response.getTelecom().get(0).getUse());
            assertEquals(org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.OTHER, response.getTelecom().get(0).getSystem());

        } catch (ConversionException e) {
            fail("could not convert Patient with just a Telecom");
        }

    }

    @Test
    void TestPatientCrossReferenceConversion() {

        net.ihe.gazelle.app.patientregistryapi.business.Patient pat = new net.ihe.gazelle.app.patientregistryapi.business.Patient();

        EntityIdentifier id = new EntityIdentifier();
        id.setSystemIdentifier("Hello_Vincent");
        id.setValue("69420");
        pat.addIdentifier(id);

        try {
            Patient response = GazelleRegistryToFhirConverter.convertPatient(pat);
            assertEquals("urn:oid:Hello_Vincent", response.getIdentifier().get(0).getSystem());
            assertEquals("69420", response.getIdentifier().get(0).getValue());

        } catch (ConversionException e) {
            fail("could not convert Patient with just a gender");
        }

    }

}
