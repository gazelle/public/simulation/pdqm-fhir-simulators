package net.ihe.gazelle.fhir.simulator.server.adapter.connector;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConversionExceptionTest {

    @Test
    public void testConversionException() {
        Assertions.assertThrows(ConversionException.class, () -> {
            throw new ConversionException();
        });
    }

    @Test
    public void testConversionException2() {
        Assertions.assertThrows(ConversionException.class, () -> {
            throw new ConversionException("test");
        });
    }

    @Test
    public void testConversionException3() {
        Assertions.assertThrows(ConversionException.class, () -> {
            throw new ConversionException("test", new Throwable());
        });
    }

    @Test
    public void testConversionException4() {
        Assertions.assertThrows(ConversionException.class, () -> {
            throw new ConversionException(new Throwable());
        });
    }

    @Test
    public void testConversionException5() {
        Assertions.assertThrows(ConversionException.class, () -> {
            throw new ConversionException("test", new Throwable(), true, true);
        });
    }

    @Test
    public void testConversionException6() {
        Assertions.assertThrows(ConversionException.class, () -> {
            throw new ConversionException("test", new Throwable(), false, false);
        });
    }

    @Test
    public void testConversionException7() {
        Assertions.assertThrows(ConversionException.class, () -> {
            throw new ConversionException("test", new Throwable(), true, false);
        });
    }

    @Test
    public void testConversionException8() {
        Assertions.assertThrows(ConversionException.class, () -> {
            throw new ConversionException("test", new Throwable(), false, true);
        });
    }

}
