package net.ihe.gazelle.fhir.simulator.server.interlay.loader;

import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.GenderCode;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistrysearchclient.adapter.PatientSearchClient;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;

public class PatientRessourceProviderServiceImplTest {

    private final PatientSearchClient patientSearchClient = mock(PatientSearchClient.class);
    private final PatientRessourceProviderServiceImpl patientRessourceProviderServiceImpl = new PatientRessourceProviderServiceImpl(patientSearchClient);

    @Test
    public void testSearchPatientsOK() throws Exception {
        SearchParameter searchParameter = new SearchParameter();
        List<SearchParameter> parameters = new ArrayList<>();
        parameters.add(searchParameter);

        Patient patient = new Patient("0", true, new Date(), null, GenderCode.MALE, 4);

        List<Patient> patients = new ArrayList<>();
        patients.add(patient);

        Mockito.when(patientSearchClient.parametrizedSearch(parameters)).thenReturn(patients);
        Assertions.assertEquals(1, patientRessourceProviderServiceImpl.searchPatients(parameters).size());

    }

    @Test
    public void testSearchPatientsKO() throws Exception {
        SearchParameter searchParameter = new SearchParameter();
        List<SearchParameter> parameters = new ArrayList<>();
        parameters.add(searchParameter);

        Mockito.when(patientSearchClient.parametrizedSearch(parameters)).thenThrow(new SearchException("Error"));
        Assertions.assertThrows(SearchException.class, () -> patientRessourceProviderServiceImpl.searchPatients(parameters));

    }


    @Test
    public void testSearchPatientsEmpty() {
        List<SearchParameter> parameters = new ArrayList<>();
        Assertions.assertThrows(SearchException.class, () -> patientRessourceProviderServiceImpl.searchPatients(parameters));

    }


}
