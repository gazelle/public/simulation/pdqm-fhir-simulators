package net.ihe.gazelle.fhir.simulator.server.interlay.interceptor;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.evsapi.client.business.ValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IHEPDQMValidatorSelectorTest {
    private IHEPDQMValidatorSelector validatorSelector;
    private RequestDetails requestDetails;

    @BeforeEach
    public void setup() {
        validatorSelector = new IHEPDQMValidatorSelector();
        requestDetails = mock(RequestDetails.class);
    }

    @Test
    public void testGetValidationServiceForReadOperation() {
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.READ);

        ValidationService validationService = validatorSelector.getValidationService(requestDetails);

        assertNotNull(validationService);
        assertEquals(validatorSelector.getValidationName(), validationService.getName());
        assertEquals(validatorSelector.getValidationValidatorRead(), validationService.getValidator());
    }

    @Test
    public void testGetValidationServiceForSearchOperation() {
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.SEARCH_TYPE);

        ValidationService validationService = validatorSelector.getValidationService(requestDetails);

        assertNotNull(validationService);
        assertEquals(validatorSelector.getValidationName(), validationService.getName());
        assertEquals(validatorSelector.getValidationValidatorSearch(), validationService.getValidator());
    }

    @Test
    public void testGetValidationServiceForOtherOperation() {
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.CREATE);

        ValidationService validationService = validatorSelector.getValidationService(requestDetails);

        assertNull(validationService);
    }
}
