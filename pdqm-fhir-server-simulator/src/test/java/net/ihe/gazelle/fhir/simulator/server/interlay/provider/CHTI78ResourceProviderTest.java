package net.ihe.gazelle.fhir.simulator.server.interlay.provider;


import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.fhir.simulator.server.interlay.loader.PatientResourceProviderService;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Patient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CHTI78ResourceProviderTest {

    private CHTI78ResourceProvider chti78ResourceProvider;
    private PatientResourceProviderService patientResourceProviderService = mock(PatientResourceProviderService.class);


    @BeforeEach
    public void testIHEITI78ResourceProvider() {
        chti78ResourceProvider = new CHTI78ResourceProvider(patientResourceProviderService);
    }

    @Test
    public void testGetResourceType() {
        Assertions.assertEquals(chti78ResourceProvider.getResourceType(), Patient.class);
    }

    @Test
    public void testSearchEmptyParameters() throws SearchException {
        Mockito.when(patientResourceProviderService.searchPatients(argThat(searchParameterList -> searchParameterList != null
                && searchParameterList.isEmpty()))).thenThrow(new SearchException("Empty search parameters"));
        Assertions.assertThrows(SearchException.class, () -> chti78ResourceProvider.findPatientsByMultipleParameters(null, null, null, null, null, null, null, null, null, null, null, null, null, null));
    }

    @Test
    public void testSearchPatients() throws SearchException {
        //Given
        Patient patient = new Patient();
        String id = "15";
        patient.setId(id);
        List<Patient> patients = new ArrayList<>();
        patients.add(patient);
        //When
        Mockito.when(patientResourceProviderService.searchPatients(argThat(searchParameterList -> searchParameterList != null
                && searchParameterList.get(0).getValue().equals(id)))
        ).thenReturn(patients);
        //Then
        Assertions.assertEquals(patient, chti78ResourceProvider.findPatientsByMultipleParameters(new StringOrListParam().add(new StringParam(id, true)), null, null, null, null, null, null, null, null, null, null, null, null, null).get(0));
    }

    @Test
    public void testSearchEmptyReturn() throws SearchException {
       //Given
        Patient patient = new Patient();
        String id = "0";
        patient.setId(id);
        List<Patient> patients = new ArrayList<>();
        //When
        Mockito.when(patientResourceProviderService.searchPatients(argThat(searchParameterList -> searchParameterList != null
                && searchParameterList.get(0).getValue().equals(id)))
        ).thenReturn(patients);
        //Then
        Assertions.assertThrows(InvalidRequestException.class, () -> chti78ResourceProvider.findPatientsByMultipleParameters(new StringOrListParam().add(new StringParam(id, true)), null, null, null, null, null, null, null, null, null, null, null, null, null));
    }

    @Test
    public void testSearchError() throws SearchException {
        Mockito.when(patientResourceProviderService.searchPatients(any())).thenThrow(new SearchException("ERROR"));
        Assertions.assertThrows(SearchException.class, () -> chti78ResourceProvider.findPatientsByMultipleParameters(null, null, null, null, null, null, null, null, null, null, null, null, null, null));

    }


    @Test
    public void testReadError() throws SearchException {

        Mockito.when(patientResourceProviderService.searchPatients(any())).thenThrow(new SearchException("ERROR"));
        Assertions.assertThrows(SearchException.class, () -> chti78ResourceProvider.findPatientById(new IdType("15")));

    }

    @Test
    public void testReadPatient() throws SearchException {
        //Given
        Patient patient = new Patient();
        String id = "15";
        patient.setId(id);
        List<Patient> patients = new ArrayList<>();
        patients.add(patient);
        //When
        Mockito.when(patientResourceProviderService.searchPatients(argThat(searchParameterList -> searchParameterList != null
                && searchParameterList.get(0).getValue().equals(id)))
        ).thenReturn(patients);
        //Then
        Assertions.assertEquals(patient, chti78ResourceProvider.findPatientById(new IdType(id)));
    }

    @Test
    public void testReadPatientEmpty() throws SearchException {
        //Given
        Patient patient = new Patient();
        String id = "0";
        patient.setId(id);
        List<Patient> patients = new ArrayList<>();
        //When
        Mockito.when(patientResourceProviderService.searchPatients(argThat(searchParameterList -> searchParameterList != null
                && searchParameterList.get(0).getValue().equals(id)))
        ).thenReturn(patients);
        //Then
        Assertions.assertThrows(InvalidRequestException.class, () -> chti78ResourceProvider.findPatientById(new IdType("15")));


    }

    @Test
    public void testReadpatientMultipleResults() throws SearchException {
        //Given
        Patient patient = new Patient();
        String id = "0";
        patient.setId(id);
        List<Patient> patients = new ArrayList<>();
        patients.add(patient);
        patients.add(patient);
        //When
        Mockito.when(patientResourceProviderService.searchPatients(argThat(searchParameterList -> searchParameterList != null
                && searchParameterList.get(0).getValue().equals(id)))
        ).thenReturn(patients);
        //Then
        Assertions.assertThrows(InvalidRequestException.class, () -> chti78ResourceProvider.findPatientById(new IdType("15")));

    }

    @Test
    public void getStringModifierEquals() {
        Assertions.assertEquals("equals", chti78ResourceProvider.getStringModifier(new StringParam("test", true)));
        Assertions.assertEquals("contains", chti78ResourceProvider.getStringModifier(new StringParam("test", false)));
    }

    @Test
    public void processIdParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processIdParam(new StringOrListParam().add(new StringParam("24", true)), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processIdParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());

    }

    @Test
    public void processActiveParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processActiveParam(new TokenOrListParam().add(new TokenParam("test")), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processActiveParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void processFamilyParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processFamilyNameParam(new StringOrListParam().add(new StringParam("test", true)), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processFamilyNameParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void processGivenNameParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processGivenNameParam(new StringOrListParam().add(new StringParam("test", true)), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processGivenNameParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void processIdentifierParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processIdentifierParam(null, searchParameterList);
        Assertions.assertEquals(0, searchParameterList.size());
        chti78ResourceProvider.processIdentifierParam(new TokenOrListParam().add(new TokenParam("test","test")), searchParameterList);
        Assertions.assertEquals(2, searchParameterList.size());

    }

    @Test
    public void processTelecomParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processTelecomParam(new TokenOrListParam().add(new TokenParam("test")), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processTelecomParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void processBirthDateParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processBirthdateParam(new DateParam("1987-11-12"), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processBirthdateParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void processAddressParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processAddressParam(new StringOrListParam().add(new StringParam("test", true)), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processAddressParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void processCityParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processCityParam(new StringOrListParam().add(new StringParam("test", true)), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processCityParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void processCountryParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processCountryParam(new StringOrListParam().add(new StringParam("test", true)), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processCountryParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }


    @Test
    public void processZipParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processPostalCodeParam(new StringOrListParam().add(new StringParam("test", true)), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processPostalCodeParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void processStateParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processStateParam(new StringOrListParam().add(new StringParam("test", true)), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processStateParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }


    @Test
    public void processGenderParam() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processGenderParam(new TokenOrListParam().add(new TokenParam("test")), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processGenderParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void processMotherMaidenName() {
        List<SearchParameter> searchParameterList = new ArrayList<>();
        chti78ResourceProvider.processMothersMaidenNameParam(new StringOrListParam().add(new StringParam("test", true)), searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
        chti78ResourceProvider.processMothersMaidenNameParam(null, searchParameterList);
        Assertions.assertEquals(1, searchParameterList.size());
    }

    @Test
    public void testSearchTooMuchResult() throws SearchException {
        List<SearchParameter> parameters = new ArrayList<>();
        Mockito.when(patientResourceProviderService.searchPatients(parameters)).thenReturn(new ArrayList<org.hl7.fhir.r4.model.Patient>() {{
            add(new Patient());
            add(new Patient());
            add(new Patient());
            add(new Patient());
            add(new Patient());
            add(new Patient());
        }});
        Assertions.assertThrows(InvalidRequestException.class, () -> chti78ResourceProvider.findPatientsByMultipleParameters(null, null, null, null, null, null, null, null, null, null, null, null, null, null));

    }

    @Test
    public void testFindPatientsByMultipleParameters() throws Exception {
        // Create mock parameters
        StringOrListParam mockId = new StringOrListParam().add(new StringParam("123", true));
        TokenOrListParam mockActive = new TokenOrListParam().add("true");
        // Add more parameters as needed...

        // Create a mock patient list
        List<Patient> mockPatients = new ArrayList<>();
        Patient mockPatient = new Patient();
        mockPatient.setId("123");
        mockPatients.add(mockPatient);

        // Define the behavior of the service
        when(patientResourceProviderService.searchPatients(anyList())).thenReturn(mockPatients);

        // Call the method under test
        List<Patient> result = chti78ResourceProvider.findPatientsByMultipleParameters(mockId, mockActive, null ,null, null, null, null, null, null, null, null, null, null, null);

        // Verify the result
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("123", result.get(0).getId());
    }

}
