package net.ihe.gazelle.fhir.simulator.server.interlay.provider;

import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.StringOrListParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import jakarta.inject.Inject;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.fhir.simulator.server.interlay.loader.PatientResourceProviderService;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import org.hl7.fhir.instance.model.api.IAnyResource;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.Patient;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a resource provider that retrieves Patient resources from a PatientResourceProviderService.
 */
public class IHETI78ResourceProvider implements IResourceProvider {


    public static final String CONTAINS = "contains";
    private static final String STRING = "String";
    private static final String EQUALS = "equals";

    protected final PatientResourceProviderService patientResourceProviderService;

    @Inject
    public IHETI78ResourceProvider(PatientResourceProviderService patientResourceProviderService) {
        this.patientResourceProviderService = patientResourceProviderService;
    }


    @Search()
    public List<Patient> findPatientsByMultipleParameters(
            @OptionalParam(name = IAnyResource.SP_RES_ID) StringOrListParam theID,
            @OptionalParam(name = Patient.SP_ACTIVE) TokenOrListParam theActive,
            @OptionalParam(name = Patient.SP_FAMILY) StringOrListParam theFamilyName,
            @OptionalParam(name = Patient.SP_GIVEN) StringOrListParam theGivenName,
            @OptionalParam(name = Patient.SP_IDENTIFIER) TokenOrListParam theIdentifier,
            @OptionalParam(name = Patient.SP_TELECOM) TokenOrListParam theTelecom,
            @OptionalParam(name = Patient.SP_BIRTHDATE) DateParam theBirthdate,
            @OptionalParam(name = Patient.SP_ADDRESS) StringOrListParam theAddress,
            @OptionalParam(name = Patient.SP_ADDRESS_CITY) StringOrListParam theCity,
            @OptionalParam(name = Patient.SP_ADDRESS_COUNTRY) StringOrListParam theCountry,
            @OptionalParam(name = Patient.SP_ADDRESS_POSTALCODE) StringOrListParam thePostalCode,
            @OptionalParam(name = Patient.SP_ADDRESS_STATE) StringOrListParam theState,
            @OptionalParam(name = Patient.SP_GENDER) TokenOrListParam theGender,
            @OptionalParam(name = "mothersMaidenName") StringOrListParam theMothersMaidenName) throws SearchException {

        List<SearchParameter> parameters = new ArrayList<>();

        processIdParam(theID, parameters);
        processActiveParam(theActive, parameters);
        processFamilyNameParam(theFamilyName, parameters);
        processGivenNameParam(theGivenName, parameters);
        processIdentifierParam(theIdentifier, parameters);
        processTelecomParam(theTelecom, parameters);
        processBirthdateParam(theBirthdate, parameters);
        processAddressParam(theAddress, parameters);
        processCityParam(theCity, parameters);
        processCountryParam(theCountry, parameters);
        processPostalCodeParam(thePostalCode, parameters);
        processStateParam(theState, parameters);
        processGenderParam(theGender, parameters);
        processMothersMaidenNameParam(theMothersMaidenName, parameters);
        List<Patient> patients = patientResourceProviderService.searchPatients(parameters);
        if (patients.isEmpty()) {
            throwEmptySearchException();
        }

        return patients;

    }

    @Read
    public Patient findPatientById(@IdParam IdType theId) throws SearchException {
        List<SearchParameter> parameters = new ArrayList<>();
        parameters.add(new SearchParameter(IAnyResource.SP_RES_ID, EQUALS, theId.getIdPart(), STRING));
        List<Patient> patients = patientResourceProviderService.searchPatients(parameters);
        if (patients.isEmpty())
            throwEmptySearchException();
        if (patients.size() > 1)
            throwMultiplePatientsFoundException();

        return patients.get(0);
    }

    public void throwEmptySearchException() {
        OperationOutcome outcome = new OperationOutcome();
        outcome.addIssue(new OperationOutcome.OperationOutcomeIssueComponent()
                .setSeverity(OperationOutcome.IssueSeverity.ERROR)
                .setCode(OperationOutcome.IssueType.NOTFOUND)
                .setDiagnostics("No matching patients")); // Set your custom error message here
        throw new InvalidRequestException("Not Found", outcome);
    }

    public void throwMultiplePatientsFoundException() {
        OperationOutcome outcome = new OperationOutcome();
        outcome.addIssue(new OperationOutcome.OperationOutcomeIssueComponent()
                .setSeverity(OperationOutcome.IssueSeverity.ERROR)
                .setCode(OperationOutcome.IssueType.NOTFOUND)
                .setDiagnostics("Multiple patients found with the same ID")); // Set your custom error message here
        throw new InvalidRequestException("More than one result", outcome);
    }


    /**
     * The getResourceType method comes from IResourceProvider, and must be overridden to indicate what type of resource this provider supplies.
     */
    @Override
    public Class<Patient> getResourceType() {
        return Patient.class;
    }


    String getStringModifier(StringParam theParam) {
        if (theParam != null && theParam.isExact()) {
            return EQUALS;
        } else {
            return CONTAINS;
        }
    }

    void processIdParam(StringOrListParam theID, List<SearchParameter> parameters) {
        if (theID != null) {
            theID.getValuesAsQueryTokens().forEach(string -> parameters.add(new SearchParameter(IAnyResource.SP_RES_ID, EQUALS, string.getValue(), STRING)));
        }
    }

    void processActiveParam(TokenOrListParam theActive, List<SearchParameter> parameters) {
        if (theActive != null) {
            theActive.getValuesAsQueryTokens().forEach(token -> parameters.add(new SearchParameter(Patient.SP_ACTIVE, EQUALS, token.getValue(), "Boolean")));
        }
    }

    void processFamilyNameParam(StringOrListParam theFamilyName, List<SearchParameter> parameters) {
        if (theFamilyName != null) {
            theFamilyName.getValuesAsQueryTokens().forEach(string ->
                    parameters.add(new SearchParameter(Patient.SP_FAMILY, getStringModifier(string), string.getValue(), STRING))
            );
        }
    }

    void processGivenNameParam(StringOrListParam theGivenName, List<SearchParameter> parameters) {
        if (theGivenName != null) {
            theGivenName.getValuesAsQueryTokens().forEach(string ->
                    parameters.add(new SearchParameter(Patient.SP_GIVEN, getStringModifier(string), string.getValue(), STRING)));
        }
    }

    void processIdentifierParam(TokenOrListParam theIdentifier, List<SearchParameter> parameters) {

        if (theIdentifier != null) {


            theIdentifier.getValuesAsQueryTokens().forEach(token -> {
                if (token.getSystem() != null && !token.getSystem().isEmpty() && !token.getSystem().isBlank())
                    parameters.add(new SearchParameter(Patient.SP_IDENTIFIER + "-domain-id", "eq", token.getSystem().replace("urn:oid:", ""), STRING));

                if (token.getValue() != null && !token.getValue().isEmpty() && !token.getValue().isBlank()) {
                    parameters.add(new SearchParameter(Patient.SP_IDENTIFIER + "-value", "eq", token.getValue(), STRING));
                }
            });
        }

    }

    void processTelecomParam(TokenOrListParam theTelecom, List<SearchParameter> parameters) {
        if (theTelecom != null) {
            theTelecom.getValuesAsQueryTokens().forEach(token ->
                    parameters.add(new SearchParameter(Patient.SP_TELECOM, "eq", token.getValue(), STRING))
            );
        }
    }

    void processBirthdateParam(DateParam theBirthdate, List<SearchParameter> parameters) {
        if (theBirthdate != null) {
            if (theBirthdate.getPrefix() != null) {
                parameters.add(new SearchParameter(Patient.SP_BIRTHDATE, theBirthdate.getPrefix().getValue(), theBirthdate.getValue().toString(), "Date"));
            } else {
                parameters.add(new SearchParameter(Patient.SP_BIRTHDATE, "eq", theBirthdate.getValue().toString(), "Date"));
            }
        }
    }

    void processAddressParam(StringOrListParam theAddress, List<SearchParameter> parameters) {
        if (theAddress != null) {
            theAddress.getValuesAsQueryTokens().forEach(string ->
                    parameters.add(new SearchParameter(Patient.SP_ADDRESS, getStringModifier(string), string.getValue(), STRING)));
        }
    }

    void processCityParam(StringOrListParam theCity, List<SearchParameter> parameters) {
        if (theCity != null) {
            theCity.getValuesAsQueryTokens().forEach(string ->
                    parameters.add(new SearchParameter(Patient.SP_ADDRESS_CITY, getStringModifier(string), string.getValue(), STRING)));
        }
    }

    void processCountryParam(StringOrListParam theCountry, List<SearchParameter> parameters) {
        if (theCountry != null) {
            theCountry.getValuesAsQueryTokens().forEach(string ->

                    parameters.add(new SearchParameter(Patient.SP_ADDRESS_COUNTRY, getStringModifier(string), string.getValue(), STRING)));

        }
    }

    void processPostalCodeParam(StringOrListParam thePostalCode, List<SearchParameter> parameters) {
        if (thePostalCode != null) {
            thePostalCode.getValuesAsQueryTokens().forEach(string ->

                    parameters.add(new SearchParameter(Patient.SP_ADDRESS_POSTALCODE, getStringModifier(string), string.getValue(), STRING)));
        }
    }

    void processStateParam(StringOrListParam theState, List<SearchParameter> parameters) {
        if (theState != null) {
            theState.getValuesAsQueryTokens().forEach(string ->

                    parameters.add(new SearchParameter(Patient.SP_ADDRESS_STATE, getStringModifier(string), string.getValue(), STRING)));
        }
    }

    void processGenderParam(TokenOrListParam theGender, List<SearchParameter> parameters) {
        if (theGender != null) {
            theGender.getValuesAsQueryTokens().forEach(token ->

                    parameters.add(new SearchParameter(Patient.SP_GENDER, "eq", token.getValue().toLowerCase(), "GenderCode")));
        }
    }

    void processMothersMaidenNameParam(StringOrListParam theMothersMaidenName, List<SearchParameter> parameters) {
        if (theMothersMaidenName != null) {
            theMothersMaidenName.getValuesAsQueryTokens().forEach(string ->

                    parameters.add(new SearchParameter("mothersMaidenName", getStringModifier(string), string.getValue(), STRING)));
        }
    }


}
