package net.ihe.gazelle.fhir.simulator.server.interlay.loader;


import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import java.util.List;

public interface PatientResourceProviderService {

    List<org.hl7.fhir.r4.model.Patient> searchPatients(List<SearchParameter> parameters) throws SearchException;


}