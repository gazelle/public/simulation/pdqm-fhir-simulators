package net.ihe.gazelle.fhir.simulator.server.adapter.servlet;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.narrative.DefaultThymeleafNarrativeGenerator;
import ca.uhn.fhir.narrative.INarrativeGenerator;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import net.ihe.gazelle.fhir.server.technical.HTTPValidatorInterceptor;
import net.ihe.gazelle.fhir.simulator.server.interlay.interceptor.IHEPDQMValidatorSelector;
import net.ihe.gazelle.fhir.simulator.server.interlay.loader.PatientRessourceProviderServiceImpl;
import net.ihe.gazelle.fhir.simulator.server.interlay.provider.IHETI78ResourceProvider;
import org.eclipse.microprofile.config.ConfigProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * This servlet is the actual FHIR server itself
 */

public class IHETI78RestfullServer extends RestfulServer {

    private static final long serialVersionUID = 1L;


    /**
     * Constructor
     */
    public IHETI78RestfullServer() {
        super(FhirContext.forR4Cached()); // This is an R4 server
    }

    /**
     * This method is called automatically when the
     * servlet is initializing.
     */
    @Override
    public void initialize() {
        /*
         * Two resource providers are defined. Each one handles a specific
         * type of resource.
         */
        List<IResourceProvider> providers = new ArrayList<>();
        providers.add(new IHETI78ResourceProvider(new PatientRessourceProviderServiceImpl()));
        setResourceProviders(providers);

        /*
         * Use a narrative generator. This is a completely optional step,
         * but can be useful as it causes HAPI to generate narratives for
         * resources which don't otherwise have one.
         */
        INarrativeGenerator narrativeGen = new DefaultThymeleafNarrativeGenerator();
        getFhirContext().setNarrativeGenerator(narrativeGen);

        /*
         * Use nice coloured HTML when a browser is used to request the content
         */


        String evsEndpoint = ConfigProvider.getConfig().getValue("evs.endpoint", String.class);
        registerInterceptor(new HTTPValidatorInterceptor(evsEndpoint, new IHEPDQMValidatorSelector()));


    }


}
