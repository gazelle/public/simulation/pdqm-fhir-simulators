package net.ihe.gazelle.fhir.simulator.server.interlay.interceptor;


import org.eclipse.microprofile.config.ConfigProvider;

public class CHPDQMValidatorSelector extends IHEPDQMValidatorSelector {

    private final String httpValidationName = ConfigProvider.getConfig().getValue("ch.http.validation.name", String.class);
    private final String httpValidationValidatorRead = ConfigProvider.getConfig().getValue("ch.http.validation.schematron.name-iti-78-read", String.class);
    private final String httpValidationValidatorSearch = ConfigProvider.getConfig().getValue("ch.http.validation.schematron.name-iti-78-search", String.class);



    @Override
    public String getValidationName() {
        return httpValidationName;
    }

   @Override public String getValidationValidatorRead() {
        return httpValidationValidatorRead;
    }

    @Override
    public String getValidationValidatorSearch() {
        return httpValidationValidatorSearch;
    }
}
