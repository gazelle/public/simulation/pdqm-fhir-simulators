package net.ihe.gazelle.fhir.simulator.server.interlay.provider;

import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import jakarta.inject.Inject;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.fhir.simulator.server.interlay.loader.PatientResourceProviderService;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import org.hl7.fhir.instance.model.api.IAnyResource;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.Patient;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a resource provider that retrieves Patient resources from a PatientResourceProviderService. This one has a limit of five results above an error with additional parameters is return. Also, the telecom parameter is not handled in this class.
 */
public class CHTI78ResourceProvider extends IHETI78ResourceProvider {
    @Inject
    public CHTI78ResourceProvider(PatientResourceProviderService patientResourceProviderService) {
        super(patientResourceProviderService);
    }

    @Override
    @Search()
    public List<Patient> findPatientsByMultipleParameters(
            @OptionalParam(name = IAnyResource.SP_RES_ID) StringOrListParam theID,
            @OptionalParam(name = Patient.SP_ACTIVE) TokenOrListParam theActive,
            @OptionalParam(name = Patient.SP_FAMILY) StringOrListParam theFamilyName,
            @OptionalParam(name = Patient.SP_GIVEN) StringOrListParam theGivenName,
            @OptionalParam(name = Patient.SP_IDENTIFIER) TokenOrListParam theIdentifier,
            @OptionalParam(name = Patient.SP_TELECOM) TokenOrListParam theTelecom,
            @OptionalParam(name = Patient.SP_BIRTHDATE) DateParam theBirthdate,
            @OptionalParam(name = Patient.SP_ADDRESS) StringOrListParam theAddress,
            @OptionalParam(name = Patient.SP_ADDRESS_CITY) StringOrListParam theCity,
            @OptionalParam(name = Patient.SP_ADDRESS_COUNTRY) StringOrListParam theCountry,
            @OptionalParam(name = Patient.SP_ADDRESS_POSTALCODE) StringOrListParam thePostalCode,
            @OptionalParam(name = Patient.SP_ADDRESS_STATE) StringOrListParam theState,
            @OptionalParam(name = Patient.SP_GENDER) TokenOrListParam theGender,
            @OptionalParam(name = "mothersMaidenName") StringOrListParam theMothersMaidenName) throws SearchException {
        List<SearchParameter> parameters = new ArrayList<>();
        processIdParam(theID, parameters);
        processActiveParam(theActive, parameters);
        processFamilyNameParam(theFamilyName, parameters);
        processGivenNameParam(theGivenName, parameters);
        processIdentifierParam(theIdentifier, parameters);
        processBirthdateParam(theBirthdate, parameters);
        processAddressParam(theAddress, parameters);
        processCityParam(theCity, parameters);
        processCountryParam(theCountry, parameters);
        processPostalCodeParam(thePostalCode, parameters);
        processStateParam(theState, parameters);
        processGenderParam(theGender, parameters);
        processMothersMaidenNameParam(theMothersMaidenName, parameters);

        List<Patient> patients = patientResourceProviderService.searchPatients(parameters);
        if (patients.isEmpty()) {
            throwEmptySearchException();
        }

        if (patients.size() > 5) {
            throwMoreThan5Result();
        }
        return patients;

    }


    private void throwMoreThan5Result() {
        OperationOutcome operationOutcome = new OperationOutcome();

        operationOutcome.addIssue(new OperationOutcome.OperationOutcomeIssueComponent()
                .setSeverity(OperationOutcome.IssueSeverity.ERROR)
                .setCode(OperationOutcome.IssueType.INCOMPLETE)
                .setDiagnostics("More than 5 results, try a more accurate search")); // Set your custom error message here
        operationOutcome.addIssue(createWarningIssue("IDRequested"));
        operationOutcome.addIssue(createWarningIssue("ActiveRequested"));
        operationOutcome.addIssue(createWarningIssue("FamilyNameRequested"));
        operationOutcome.addIssue(createWarningIssue("GivenNameRequested"));
        operationOutcome.addIssue(createWarningIssue("IdentifierRequested"));
        operationOutcome.addIssue(createWarningIssue("TelecomRequested"));
        operationOutcome.addIssue(createWarningIssue("BirthdateRequested"));
        operationOutcome.addIssue(createWarningIssue("AddressRequested"));
        operationOutcome.addIssue(createWarningIssue("CityRequested"));
        operationOutcome.addIssue(createWarningIssue("CountryRequested"));
        operationOutcome.addIssue(createWarningIssue("PostalCodeRequested"));
        operationOutcome.addIssue(createWarningIssue("StateRequested"));
        operationOutcome.addIssue(createWarningIssue("GenderRequested"));
        operationOutcome.addIssue(createWarningIssue("MothersMaidenNameRequested"));
        throw new InvalidRequestException("More than 5 result", operationOutcome);
    }


    public OperationOutcome.OperationOutcomeIssueComponent createWarningIssue(String message) {
        return new OperationOutcome.OperationOutcomeIssueComponent().setSeverity(OperationOutcome.IssueSeverity.WARNING).setCode(OperationOutcome.IssueType.INCOMPLETE).setDiagnostics(message);
    }


}

