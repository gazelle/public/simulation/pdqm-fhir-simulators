package net.ihe.gazelle.fhir.simulator.server.interlay.loader;

import jakarta.enterprise.context.ApplicationScoped;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistrysearchclient.adapter.PatientSearchClient;
import net.ihe.gazelle.fhir.simulator.server.adapter.connector.GazelleRegistryToFhirConverter;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import org.eclipse.microprofile.config.ConfigProvider;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class PatientRessourceProviderServiceImpl implements PatientResourceProviderService {


    private static final GazelleLogger logger = GazelleLoggerFactory.getInstance().getLogger(PatientRessourceProviderServiceImpl.class);
    String patientRegistryUrl;
    private PatientSearchClient client;


    public PatientRessourceProviderServiceImpl() {
    }

    PatientRessourceProviderServiceImpl(PatientSearchClient patientSearchClient) {
        this.client = patientSearchClient;
    }


    /**
     * Initialize the Search Client using the Operational Preferences Service.
     *
     * @throws SearchException if the service cannot be instantiated.
     */
    void initializeSearchClient() throws SearchException {
        try {
            patientRegistryUrl = ConfigProvider.getConfig().getValue("patient.registry.service", String.class);
            this.client = new PatientSearchClient(new URL(patientRegistryUrl));
        } catch (MalformedURLException e) {
            logger.error("Error while creating the URL for the Patient Registry");
            throw new SearchException(e);
        }

    }


    public List<org.hl7.fhir.r4.model.Patient> searchPatients(List<SearchParameter> parameters) throws SearchException {

        if (parameters.isEmpty())
            throw new SearchException("Empty search parameters");

        if (client == null) {
            initializeSearchClient();
        }
        List<org.hl7.fhir.r4.model.Patient> resources = new ArrayList<>();
        List<Patient> patients = client.parametrizedSearch(parameters);

        for (Patient pat : patients) {
            try {

                resources.add(GazelleRegistryToFhirConverter.convertPatient(pat));
            } catch (Exception e) {
                logger.error("Error while converting patient to FHIR model :"+e.getMessage());
            }

        }
        return resources;
    }



}
