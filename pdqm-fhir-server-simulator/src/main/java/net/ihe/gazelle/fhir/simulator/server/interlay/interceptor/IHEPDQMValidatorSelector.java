package net.ihe.gazelle.fhir.simulator.server.interlay.interceptor;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.evsapi.client.business.ValidationService;
import net.ihe.gazelle.fhir.server.business.ValidatorServiceMapper;
import org.eclipse.microprofile.config.ConfigProvider;

public class IHEPDQMValidatorSelector implements ValidatorServiceMapper {

    private final String httpValidationName = ConfigProvider.getConfig().getValue("ihe.http.validation.name", String.class);
    private final String httpValidationValidatorRead = ConfigProvider.getConfig().getValue("ihe.http.validation.schematron.name-iti-78-read", String.class);
    private final String httpValidationValidatorSearch = ConfigProvider.getConfig().getValue("ihe.http.validation.schematron.name-iti-78-search", String.class);

    @Override
    public ValidationService getValidationService(RequestDetails requestDetails) {
        String validationName = getValidationName();
        if (requestDetails.getRestOperationType().equals(RestOperationTypeEnum.READ)) {
            String httpValidationValidator = getValidationValidatorRead();
            return new ValidationService().setValidator(httpValidationValidator).setName(validationName);
        }

        if (requestDetails.getRestOperationType().equals(RestOperationTypeEnum.SEARCH_TYPE)) {
            String httpValidationValidator = getValidationValidatorSearch();
            return new ValidationService().setValidator(httpValidationValidator).setName(validationName);
        }

        return null;
    }

    public String getValidationName() {
        return httpValidationName;
    }

    public String getValidationValidatorRead() {
        return httpValidationValidatorRead;
    }

    public String getValidationValidatorSearch() {
        return httpValidationValidatorSearch;
    }
}

