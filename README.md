# pdqm-fhir-simulator
This project is composed of two subprojects:
- pdqm-fhir-server-simulator
- pdqm-fhir-client-simulator

## pdqm-fhir-server-simulator
This project uses Quarkus, the Supersonic Subatomic Java Framework.

PDQm manage one kind of resource : `Patient`.
This project handles two kinds of requests:

- `GET /Patient/{id}` : Get a patient by id.
- `GET/POST /Patient?` : Search for patients.

THe app has two servlets:
- `/pdqm-fhir-simulator/CH` : The CH servlet is used to simulate the CH FHIR server.
- `/pdqm-fhir-simulator/IHE` : The IHE servlet is used to simulate the IHE FHIR server.


## pdqm-fhir-client-simulator
To be defined.